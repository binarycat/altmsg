package altmsg

import (
	"testing"
)

func TestMarshalEvent(t *testing.T) {
	ev1 := NewEvent(&EventChat{ Text: "hello!" })
	raw1, err := MarshalEvent(ev1)
	if err != nil { t.Fatal("marshal error: ", err) }
	res1, err := UnmarshalEvent(raw1)
	if err != nil { t.Fatal("unmarshal error: ", err) }
	if res1.Body.(*EventChat).Text != "hello!" {
		t.Fatal("text mismatch")
	}
}
