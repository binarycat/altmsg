package altmsg

import "encoding/json"

type EventBody interface {
	Type() string
	// creates a new empty event of the same type as this event
	New() EventBody
}

var EventTypeMap = map[string]EventBody{
	"chat": &EventChat{},
	"file": &EventFile{},
}

/*type EventJSON map[string]any

func (e EventJSON) Type() string {
	ty, ok := e["type"]
	if !ok {
		return ""
	}
	return ty
}

func (e EventJSON) New() Event {
	return make(EventJSON)
    }*/

type EventChat struct {
	Text string `json:"text"`
}

func (e *EventChat) Type() string {
	return "chat"
}

func (e *EventChat) New() EventBody {
	return &EventChat{}
}

type EventFile struct {
	Filename string `json:"filename"`
	Filetype string `json:"filetype"`
	File []byte `json:"file"`
}

func (e *EventFile) Type() string {
	return "file"
}

func (e *EventFile) New() EventBody {
	return &EventFile{}
}

// events are contained within the encrypted body of messages
type Event struct {
	Type string `json:"type"`
	// raw json value for the body of the event.
	// may be nil.  used during (un)marshaling.
	Raw json.RawMessage `json:"data"`
	Body EventBody `json:"-"`
}

func NewEvent(eb EventBody) *Event {
	return &Event{
		Type: eb.Type(),
		Body: eb,
	}
}

func MarshalEvent(ev *Event) ([]byte, error) {
	var err error
	ev.Raw, err = json.Marshal(ev.Body)
	if err != nil { return nil, err }
	return json.Marshal(ev)
}

func UnmarshalEvent(src []byte) (*Event, error) {
	ev := &Event{}
	err := json.Unmarshal(src, ev)
	if err != nil { return nil, err }
	et := EventTypeMap[ev.Type]
	if et == nil {
		// unknown event type is not an error
		return ev, nil
	}
	ev.Body = et.New()
	err = json.Unmarshal(ev.Raw, ev.Body)
	if err != nil { return nil, err }
	return ev, nil
}

