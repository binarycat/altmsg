// simple proof-of-concept for a messaging system that can endure server blackouts due to using multiple midbox servers
package altmsg

import (
	"time"
	"encoding/json"
	"net/http"
	"errors"
	"fmt"
	"bytes"
	
	"git.envs.net/binarycat/altmsg/auth"
)

// MsgCore consists of all the fields used to compute the signature.
type MsgCore struct {	
	// timestamp added by the sending client, specifies when a message was sent.  also used in the computation of the message signature, serving to prevent replay attacks
	Sent time.Time
	From, To auth.Pub
	// encrypted body of message
	RawBody []byte
}

// a Msg is sent between clients via queue servers.
// a message consists of 3 layers:
// 1. envelope: unencypted fields
// 2. event header: common fields used by all event types
// 3. event body: fields specific to a given event type
type Msg struct {
	MsgCore
	// timestamp added by the server, specifies when a message was recieved
	// TODO: fix typo
	Recieved time.Time `json:,omitempty`
	// signature that verifies the account sending the message is who they say they are
	Sig []byte
}

func NewMsg(from auth.Priv, to auth.Pub, body []byte) (*Msg, error) {
	msg := &Msg{}
	msg.Sent = time.Now()
	msg.From = from.Pub()
	msg.To = to
	enc, err := auth.Encrypt(to, body)
	if err != nil { return nil, err }
	msg.RawBody = enc
	j, err := json.Marshal(msg.MsgCore)
	if err != nil { return nil, err }
	sig, err := auth.Sign(from, j)
	if err != nil { return nil, err }
	msg.Sig = sig
	return msg, nil
}

func NewMsgEvent(from auth.Priv, to auth.Pub, eb EventBody) (*Msg, error) {
	body, err := MarshalEvent(NewEvent(eb))
	if err != nil { return nil, err }
	return NewMsg(from, to, body)
}

// verify the message signature
// called when a message is recieved by a queue server, then again by the reciving client
func (msg *Msg) Verify() error {
	j, err := json.Marshal(msg.MsgCore)
	if err != nil { return err }
	return auth.Verify(msg.From, j, msg.Sig)
}

var ErrMsgPush = errors.New("failed to push message to server")

func (msg *Msg) Push(u string) error {
	msgJSON, err := json.Marshal(msg)
	if err != nil { return err }
	resp, err := http.Post(fmt.Sprintf("%sv0/push", u),
		"application/json", bytes.NewReader(msgJSON))
	if err != nil { return err }
	if resp.StatusCode != http.StatusOK {
		return ErrMsgPush
	}
	return nil
}

func (msg *Msg) Decrypt(k auth.Priv) ([]byte, error) {
	return auth.Decrypt(k, msg.RawBody)
}

func (msg *Msg) Event(k auth.Priv) (*Event, error) {
	raw, err := msg.Decrypt(k)
	if err != nil { return nil, err }
	return UnmarshalEvent(raw)
}
