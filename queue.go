package altmsg

import (
	"encoding/json"
	"net/http"
	"net/url"
	"log"
	"time"
	"sync"

	"git.envs.net/binarycat/altmsg/auth"
)

// PullReq represents an request to /v0/pull
// one-letter field names are used to minimize the size of json requests
type PullReq struct {
	// request messages sent to this key
	K auth.Pub
	// only return messages the server recieved after this timestamp
	T time.Time
	// max number of messages to return after and before timestamp
	// usually one of these is set to zero (this odd api allows retrieving messages either oldest-first or newest-first with very little increase in code complexity)
	// here "before" actually means "not after", so a message sent exactly at T will count as before T.
	A, B int
	// TODO: this should include a challenge to prove that the request comes from someone who actually has the corrosponding private key (the messages will still be safe, but without this an attacker could easily see how often two ppl are communicating)

}

type PullResp struct {
	M []*Msg
}

type ListenReq struct {
	K auth.Pub
	T time.Time
	I time.Duration
}

type Queue struct {
	M []*Msg
}

// find the index of the earliest message that was recived after t
func (q *Queue) TimestampIdx(t time.Time) int {
	// (could use binary search here to make it go faster)
	i := len(q.M) - 1
	for i >= 0 {
		if !q.M[i].Recieved.After(t) {
			i += 1
			break
		}
		i -= 1
	}
	return i
}

type QueueSrv struct {
	QueueLock sync.RWMutex
	// maps the public key fingerprint of the recipeint to a list of messages sent to that public key.
	Queues map[string]Queue
	// api root
	URL string
}

func NewQueueSrv() *QueueSrv {
	return &QueueSrv{
		Queues: make(map[string]Queue),
	}
}


func (qs *QueueSrv) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/v0/push" {
		if r.Method != "POST" {
			goto BadRequest
		}
		decoder := json.NewDecoder(r.Body)
		var msg = &Msg{}
		err := decoder.Decode(msg)
		if err != nil {
			log.Print("json decoding error: ", err)
			goto BadRequest
		}
		err = msg.Verify()
		if err != nil {
			log.Print("message verification error: ", err)
			goto BadRequest
		}
		msg.Recieved = time.Now()
		qs.QueueLock.Lock()
		defer qs.QueueLock.Unlock()
		fing := msg.To.Fingerprint()
		q := qs.Queues[fing]
		// TODO: require the recipent to open an inbox with the queue server before it can recieve messages? (anti-spam measure)
		q.M = append(q.M, msg)
		qs.Queues[fing] = q
		w.WriteHeader(http.StatusOK)
	} else if r.URL.Path == "/v0/pull" {
		if r.Method != "POST" {
			log.Print("unexpected method: ", r.Method)
			goto BadRequest
		}
		decoder := json.NewDecoder(r.Body)
		var pull PullReq
		err := decoder.Decode(&pull)
		if err != nil {
			log.Print("json decoding error: ", err)
			goto BadRequest
		}
		// TODO: challenge to prove requester has private key
		fing := pull.K.Fingerprint()
		q := qs.Queues[fing]
		i := q.TimestampIdx(pull.T)
		a := i + pull.A + 1
		b := i - pull.B
		if b < 0 { b = 0 }
		if a > len(q.M) { a = len(q.M) }
		pullResp := PullResp{ M: q.M[b:a] }
		encoder := json.NewEncoder(w)
		err = encoder.Encode(pullResp)
		if err != nil {
			log.Print("json decoding error: ", err)
		}
	} else if r.URL.Path == "/v0/listen" {
		if r.Method != "POST" {
			goto BadRequest
		}
		decoder := json.NewDecoder(r.Body)
		var listen ListenReq
		err := decoder.Decode(&listen)
		if err != nil {
			log.Print("json decoding error: ", err)
			goto BadRequest
		}
		//fing := .Fingerprint()
	} else {
		http.NotFound(w, r)
	}
	return
BadRequest:
	w.WriteHeader(http.StatusBadRequest)
}

func (qs *QueueSrv) Listen(rawURL string) error {
	// TODO: error if qs is alreading listening on a different address
	u, err := url.Parse(rawURL)
	if err != nil { return err }
	qs.URL = rawURL
	if u.Scheme == "http" {
		// TODO: rawURL MUST end in '/'
		go http.ListenAndServe(u.Host, qs)
	} else {
		panic("https not implemented!")
	}
	return nil
}
