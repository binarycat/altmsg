package auth

import (
	"crypto/rsa"
	"crypto/sha256"
	"crypto/rand"
	"crypto"
	"fmt"
)

type Pub struct {
	// this field is left exported so that json marshaling can work
	RSA *rsa.PublicKey
}

// used internally, not part of the protocol spec
func (p Pub) Fingerprint() string {
	return fmt.Sprintf("%d,%d", p.RSA.N, p.RSA.E)
}

type Priv struct {
	RSA *rsa.PrivateKey
}

func (p Priv) Pub() Pub {
	return Pub{ RSA: p.RSA.Public().(*rsa.PublicKey) }
}

func New() (Priv, error) {
	k, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil { return Priv{}, err }
	return Priv{ RSA: k }, nil
}

func Encrypt(pub Pub, msg []byte) ([]byte, error) {
	return rsa.EncryptOAEP(sha256.New(), rand.Reader, pub.RSA, msg, []byte("altmsg"))
}

func Decrypt(priv Priv, msg []byte) ([]byte, error) {
	return rsa.DecryptOAEP(sha256.New(), rand.Reader, priv.RSA, msg, []byte("altmsg"))
}

func Sign(priv Priv, msg []byte) ([]byte, error) {
	digest := sha256.Sum256(msg)
	return rsa.SignPSS(rand.Reader, priv.RSA, crypto.SHA256, digest[:], nil)
}

func Verify(pub Pub, msg, sig []byte) error {
	digest := sha256.Sum256(msg)
	return rsa.VerifyPSS(pub.RSA, crypto.SHA256, digest[:], sig, nil)
}
