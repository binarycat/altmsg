package auth

import (
	"testing"
)

func TestEncrypt(t *testing.T) {
	key, err := New()
	if err != nil { t.Fatal("keygen failed: ", err) }
	enc, err := Encrypt(key.Pub(), []byte("foobar"))
	if err != nil { t.Fatal("encryption failed: ", err) }
	dec, err := Decrypt(key, enc)
	if err != nil { t.Fatal("decryption failed: ", err) }
	if string(dec) != "foobar" {
		t.Fatal("round trip changed contents")
	}
}

func TestVerify(t *testing.T) {
	key, err := New()
	if err != nil { t.Fatal("keygen failed: ", err) }
	sig, err := Sign(key, []byte("baz"))
	if err != nil { t.Fatal("signing failed: ", err) }
	err = Verify(key.Pub(), []byte("baz"), sig)
	if err != nil { t.Fatal("verification failed: ", err) }
}
