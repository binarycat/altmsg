package altmsg

import (
	"encoding/json"
	"net/http"
	"errors"
	"fmt"
	"bytes"
)

var ErrStatusCode = errors.New("unexpected status code")

func DoPullFrom(u string, req *PullReq) (*PullResp, error) {
	reqJSON, err := json.Marshal(req)
	if err != nil { return nil, err }
	resp, err := http.Post(fmt.Sprintf("%sv0/pull", u),
		"application/json", bytes.NewReader(reqJSON))
	if err != nil { return nil, err }
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%w: %d", ErrStatusCode, resp.StatusCode)
	}
	dec := json.NewDecoder(resp.Body)
	pullResp := &PullResp{}
	err = dec.Decode(&pullResp)
	if err != nil { return nil, err }
	return pullResp, nil
}
