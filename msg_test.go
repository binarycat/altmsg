package altmsg

import (
	"testing"

	"git.envs.net/binarycat/altmsg/auth"
)

func TestVerify(t *testing.T) {
	from, err := auth.New()
	if err != nil { t.Fatal("keygen failed: ", err) }
	to, err := auth.New()
	if err != nil { t.Fatal("keygen failed: ", err) }
	msg, err := NewMsgEvent(from, to.Pub(),
		&EventChat{Text: "this is a test message"})
	if err != nil { t.Fatal("message creation failed: ", err) }
	err = msg.Verify()
	if err != nil { t.Fatal("message verification failed: ", err) }
	ev, err := msg.Event(to)
	if err != nil { t.Fatal("failed to decrypt event: ", err) }
	if ev.Type != "chat" {
		t.Fatal("unexpected event type: ", ev.Type)
	}
	if ev.Body.(*EventChat).Text != "this is a test message" {
		t.Fatal("text mismatch")
	}
}
