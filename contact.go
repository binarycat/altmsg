package altmsg

import (
	"log"
	
	"git.envs.net/binarycat/altmsg/auth"
)

// public contact info.
type Contact struct {
	// nickname
	N string
	// public key
	K auth.Pub
	// list of "push addresses"
	// either points to a queue server, or directly to a p2p cabable client.
	P []string
	// list of urls account info can be retrieved/updated from, in order of preference
	U []string
}

// send a message to each push address.
// return the number of successfully delivered messages.
func (c *Contact) Send(from auth.Priv, eb EventBody) (int, error) {
	msg, err := NewMsgEvent(from, c.K, eb)
	if err != nil { return 0, err }
	success := 0
	for _, addr := range c.P {
		// TODO: more advanced error handling?  maybe somehow return all errors?
		err = msg.Push(addr)
		if err != nil {
			log.Printf("failed to push message to '%s': %e", addr, err)
		} else {
			success += 1
		}
	}
	if success == 0 { return 0, err }
	return success, nil
}

